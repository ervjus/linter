//now it load express module with `require` directive
const express = require("express");
const app = express();
const port = process.env.port || 8080;
//Define request response in root URL (/) and response with text Hello World!
app.get("/", function (req, res) {
  res.send("Terveppä terve");
});
//Launch listening server on port 8080 and consoles the log.
app.listen(port, function () {
  console.log("app listening on port 8080!");
});