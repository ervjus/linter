FROM node:20-alpine

WORKDIR /app

COPY ./package*.json .
RUN npm install

COPY ./index.js .
ARG APP_VERSION
ENV VERSION=${APP_VERSION}
EXPOSE 8080
CMD ["node", "./index.js"]
